//C stuff
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

//C++ stuff
#include<string>
#include<iostream>
using namespace std;
#include "http.h"

//the port client will be connecting to
#define PORT 6667
//max number of bytes we can get at once
#define MAXDATASIZE 1024
#define SILENT 0
#define defaultNICKNAME "Translate"
#define VERSION "0.3.1"

char* NICKNAME=defaultNICKNAME;

int isstringempty(const string& s)
{
	for(int i=0;i<s.length();i++)
		if(s[i]!=' ') return 0;
	return 1;
}

string trim(const string& s)
{
	string result(""),tmp("");
	bool firstchar(false);
	
	for(int i=0;i<s.length();i++)
	{
		if(s[i]!=' ') firstchar=true;
		if(firstchar==true) tmp=tmp+s[i];
	}
	
	firstchar=false;
	for(int i=tmp.length()-1;i>=0;i--)
	{
		if(tmp[i]!=' ') firstchar=true;
		if(firstchar==true) result=tmp[i]+result;
	}
	return result;
}

string stringreplace(const string& source,const string& del, const string& replacement)
{
	unsigned int start(0);
	string result(source);
	while( (start=result.find(del)) != string::npos )
	{
		result=result.replace(start,del.length(),replacement);
	}
	return result;
}

string stringreplace(const string& source,char* del, char* replacement)
{
	string sdel(del),sreplacement(replacement);
	return stringreplace(source,sdel,sreplacement);
}

void tidymeaning(string& meaning)
{
	//string meaning(cmeaning);
	string sub[5];
	string tmp,output;

	size_t pos1=0,pos2; //get the four different substrings
	for(int i=0;i<5;i++)
	{
		pos2=meaning.find('.', pos1);
		sub[i]=meaning.substr(pos1,pos2-pos1);
		pos1=pos2+1;
	}
	
	//check which ones are empty
	for(int i=0;i<5;i++)
	{
		pos1 = sub[i].find('=');
		pos2 = sub[i].length()-pos1;
		tmp = sub[i].substr(pos1+1, pos2);
		if(!isstringempty(tmp)) 
			output += trim(sub[i].substr(0,pos1-1)) + ": " + trim(sub[i].substr(pos1+1,pos2)) + ". ";
	}
	
	meaning=output;
	return;
}
 
 /*this function will be replaced as soon as the wiki is updated to use the mediawiki API*/
string botparsehtml(string meaning)
{
	string result("");

	int flag=0;
	char* retvalue;

	if(trim(meaning)=="") 
	{
		printf("MEANING==""!\n");
		return string("");
	}
	
	int start=meaning.find("{{");
	if(start==string::npos)
	{
		printf("couldn't find {{\n");
		return string("");
	}
	
	start=meaning.find("|", start+1);
	//start=strstr(start,"|")+1;
	start=meaning.find("|", start+1);
	//start=strstr(start,"|");
	
	start++;
	
	int end=meaning.find("}}",start);
	//end=strstr(start,"}}");

	if(end==string::npos)
	{
		printf("couldn't find }}\n");
		return NULL;
	}

	while(start!=end)
	{
		if(flag==0)
			if(meaning[start]=='<')
			{
				flag=1;
				start++;
			}	
			else
			{
				if(meaning[start]=='\n') result+=' ';
				else if(meaning[start]=='|') result+='.';
				else result+=meaning[start];
				start++;
			}
		else 
		{
			if(meaning[start]=='>') flag=0;
			start++;
		}
	}

	return result;
}

void handlemsgs(char* txt, int sockfd)
{

	char* source=NULL; //where did the msg come from?
	char* type=NULL; //what kind of IRC command is it?
	char* target=NULL; //was the msg sent to me or a channel or....
	char* tmp; //where we save what we are parsing
	char* pos; //pointer to a temporary position in a string.
	static char buf[MAXDATASIZE];

	tmp=txt;
	while(tmp[0]==' ') 
		tmp++; //skip all the initial spaces

	if(tmp[0]==':')//the msg source is specified. move it somewhere.
	{
		tmp++; //ignore the ':'

		pos=strchr(tmp, ' ');
		if(pos!=NULL) *pos='\0';
		source=tmp;
		tmp=pos+1;
		
		for(int i=0;i<strlen(source);i++)
			if(source[i]=='!') source[i]='\0';
	}
	
	pos=strchr(tmp, ' ');
	if(pos!=NULL) *pos='\0';
	type=tmp;
	tmp=pos+1;
	
	if(strcmp(type,"PRIVMSG")==0)
	{
		pos=strchr(tmp, ' ');
		if(pos!=NULL) *pos='\0';
		target=tmp;
		tmp=pos+1;
		
		while(*tmp!=':')
			tmp++;
		tmp++; //skip till after the ":"
		if( strncmp(tmp,NICKNAME,strlen(NICKNAME))==0 || strncmp(target,NICKNAME,strlen(NICKNAME))==0 ) //we are being addressed. in a channel or privately
		{
			if(strncmp(tmp,NICKNAME,strlen(NICKNAME))==0) tmp+=strlen(NICKNAME)+1; //if in a channel remove my nickname.
			
			while(*tmp==' ') tmp++;
			char* remove=tmp; //remove the '\r\n' from the end.
			while(*remove!='\0')
			{
				if(!((*remove>='A' && *remove<='Z') || (*remove>='a' && *remove<='z') || (*remove>='0' && *remove<='9') || *remove=='\r' || *remove=='\n' || *remove==' '))
				{
					sprintf(buf, "PRIVMSG %s :Invalid Term. Use letters and numbers only.\r\n\0",target);
					send(sockfd, buf, strlen(buf), 0);
					return;
				}
				if(*remove=='\r' || *remove=='\n') *remove='\0';
				remove++;
			}
			string word(tmp);
			word=trim(tmp);
			word=stringreplace(word," ","%20");
			
			sprintf(buf, "http://wiki.arabeyes.org/api.php?action=query&prop=revisions&titles=API|techdict:%s&rvprop=timestamp|user|comment|content&format=xmlfm",word.c_str() );
			
			char* meaning=get_webpage(buf);
			int count=0;
			while(meaning==NULL && count<10)
			{
				printf("%d. failed to retrive data from url. trying again: %s\n",count,buf);
				meaning=get_webpage(buf);
				count++;
			}
			if(meaning==NULL)
			{
				sprintf(buf, "PRIVMSG %s :Unable to retrive data from server\r\n\0",target);				
			}
			else
			{
				string parsedmeaning=botparsehtml(string(meaning));
				freewebpage(meaning);
				if(trim(parsedmeaning)=="")
					sprintf(buf, "PRIVMSG %s :Term not found.\r\n\0",target);
				else
				{
					tidymeaning(parsedmeaning);
					if(*target=='#')
						sprintf(buf, "PRIVMSG %s :%s, %s\r\n\0",target,source,parsedmeaning.c_str());
					else
						sprintf(buf, "PRIVMSG %s :%s\r\n\0",source,parsedmeaning.c_str());
				}
//				free(parsedmeaning);
			}
			send(sockfd, buf, strlen(buf), 0);

		}
	} else if(strcmp(type,"PING")==0)
	{
	
		while(*tmp!=':')
			tmp++;
		tmp++; //skip till after the ":"
	target=tmp;
	sprintf(buf, "PONG %s\r\n\0",target);
	send(sockfd, buf, strlen(buf), 0);
	printf("%s",buf);
		
		
	}
}
 
char* rcv(int sockfd)
{
	static char buf[MAXDATASIZE];
	char* rtnvalue;
	int numbytes;
	if((numbytes = recv(sockfd, buf, MAXDATASIZE-1, 0)) == -1)
	{
		perror("recv()");
		exit(1);
	}
	else
		printf("");//"Client-The recv() is OK...\n");
 
	buf[numbytes] = '\0';
	
	if(!SILENT) printf("%s", buf);
	rtnvalue=(char*) malloc( sizeof(char)*(strlen(buf)+1) );
	strcpy(rtnvalue,buf);
	return rtnvalue;
}
int main(int argc, char *argv[])
{
	int sockfd, numbytes;
	char *buf;
	struct hostent *he;
	buf=(char*)malloc(sizeof(char)*MAXDATASIZE);
	//connector’s address information
	struct sockaddr_in their_addr;
 
	//if no command line argument supplied
	if(argc < 3)
	{
		fprintf(stderr, "Client-Usage: %s [Irc (--nick=[nickname]) [server address] [channel name]\n Channel needs to be written without an #.\nNickname defaults to Translate.\n", argv[0]);
		exit(1);
	}
 
	//get the host info
	for(int i=1;i<argc-2;i++)
		if(strncmp("--nick=",argv[i],7)==0)
		{
			NICKNAME=(char*)malloc( sizeof(char)*(strlen(argv[i])-6) );
			strcpy(NICKNAME,argv[i]+7);
		}
		else if(strncmp("--version",argv[i],9))
		{
			cout << "Translate 0.3.1" << endl;
		}
		else
		{
			fprintf(stderr,"Invalid argument %s.\n", argv[i]);
			exit(1) ;
		}
	
	if((he=gethostbyname(argv[argc-2])) == NULL)
	{
		perror("gethostbyname()");
		exit(1);
	}
	else
		printf("Client-The remote host is: %s\n", argv[argc-2]);
 
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("socket()");
		exit(1);
	}
	else
		printf("Client-The socket() sockfd is OK...\n");
 
	//host byte order
	their_addr.sin_family = AF_INET;
	//short, network byte order
	printf("Server-Using %s and port %d...\n", argv[argc-2], PORT);
	their_addr.sin_port = htons(PORT);
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	//zero the rest of the struct
	memset(&(their_addr.sin_zero), '\0', 8);
 
	if(connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1)
	{
		perror("connect()");
		exit(1);
	}
	else
		printf("");//"Client-The connect() is OK...\n");

/************** START INTERACTING WITH A SERVER ***********/

sprintf(buf, "PASS stupidbot123\r\nUSER AfiefBot 0 * :Afiefbot 0.1\r\n\0");
send(sockfd, buf, strlen(buf), 0);

sprintf(buf, "NICK %s\r\n\0", NICKNAME);
send(sockfd, buf, strlen(buf), 0);

sprintf(buf, "JOIN #%s\r\n\0",argv[argc-1]);
send(sockfd, buf, strlen(buf), 0);


while(1)
{
	char *recieved,query;
	recieved=rcv(sockfd);
	handlemsgs(recieved,sockfd);
	free(recieved);
//	sleep(2);
}

	printf("Client-Closing sockfd\n");
	close(sockfd);
	return 0;
}
