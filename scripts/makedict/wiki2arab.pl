#!/usr/bin/perl
########################################################################
# wiki2arab.pl
# English Arabic Technical Computing Dictionary
# Arabeyes Arabisation Team 
# Klaus Lagally, U Stuttgart
# lagallyk@acm.org
# May 28, 2007
########################################################################
# write data and version file
# read and tag dictionary data
# sort Arabic entries
# process driver file to PDF
########################################################################

use strict;

open (RAWTECH, shift(@ARGV)) or die("input file not found\n");

# intermediate files
my $entries = "dictdata.dat";	# tagged data
my $keyed   = "arabdata.key";	# keyed data
my $sorted  = "arabdata.srt";	# sorted data

my $sortok  = 1;		# set to 0 if the files are too large
				# for the OS sorting routine

print "*******************************************************************\n";
print "Wiki2Arab: new Arabic version of the English-Arabic Technical Dictionary\n";
print "*******************************************************************\n";
my $VERSION = "2.0";
# change this! KL

#get time to be used in version string.
my ($sec,$min,$hour,$mday,$mon,$year,
          $wday,$yday,$isdst) = localtime time;

$mon += 1;
$year += 1900;
$mon = sprintf("%02d", $mon);
$mday = sprintf("%02d", $mday);

########################################################################
#output the version

print "*******************************************************************\n";
print "compute the version string ...\n";
print "*******************************************************************\n";
open (HANDLE, ">dictvers.tex");
print HANDLE "\\def \\Version{Version: $VERSION.$mday-$mon-$year}\n";
close (HANDLE);

########################################################################
# write tagged data

print "*******************************************************************\n";
print "convert input data ...\n";
print "*******************************************************************\n";
open (HANDLE, ">$entries");

#reading the entries
my $line;
while ($line = <RAWTECH>)
{
	my @words = split(/====/, $line);
	my $en = $words[0] ? $words[0] : "-";
	my $ar = $words[1] ? $words[1] : "-";
	
	#encode the arabic tex as arabtex and output it.
	$ar =~ s/\\n|::|،|\/|\|/, /g;
	$ar =~ s/see(.*)\n/\<see$1\>\n/g;
	chop ($ar);

	print HANDLE "\\Entry{\\En{$en}, \\Ar{$ar}}\n";
}
print HANDLE "\n";
close (HANDLE);

########################################################################
# compile English-Arabic dictionary

print "*******************************************************************\n";
print "build English-Arabic PDF file ... wait ...\n";
print "*******************************************************************\n";
print qx(pdflatex techdict.tex);

########################################################################
# add keys and headers

print "*******************************************************************\n";
print "add sorting keys and header info ...\n";
print "*******************************************************************\n";
# set parameters
open (HANDLE, ">addkeys.ctl");
#print HANDLE "mydata2.tex mydata2.key \n";
print HANDLE "$entries $keyed \n";
close (HANDLE);

print qx(latex addkeys.tex);

########################################################################
# sort the data

print "*******************************************************************\n";
print "sort the Arabic records ...\n";
print "*******************************************************************\n";
print "File $keyed to File $sorted\n";

if ($sortok == 1) {# use OS sorting routine
	print qx(sort $keyed >$sorted);}

else {# set parameters
	open (HANDLE, ">keysort.ctl");
	print HANDLE "$keyed $sorted \n)";
	close (HANDLE);
	print qx(latex keysort.tex);
	# remove ancillary files
	print qx(rm keysort.1);
	print qx(rm keysort.2);
	print qx(rm keysort.3);
	print qx(rm keysort.4);}

########################################################################
# compile Arabic-English dictionary

print "*******************************************************************\n";
print "build Arabic-English PDF file ... wait ...\n";
print "*******************************************************************\n";
#print qx(pdflatex arabdict.tex);
#qx(pdflatex arabdict2.tex);

########################################################################
print "*******************************************************************\n";
print "Wiki2Arab done.\n";
print "*******************************************************************\n";
exit;
########################################################################






