#!/usr/bin/perl
########################################################################
# dict2arab.pl
# English Arabic Technical Computing Dictionary
# Arabeyes Arabisation Team 
# Klaus Lagally, U Stuttgart
# lagallyk@acm.org
# Nov 08, 2007
########################################################################
# write data and version file
# read and tag dictionary data
# sort Arabic entries
# process driver file to PDF
########################################################################

use strict;

open (RAWTECH, shift(@ARGV)) or die("input file not found\n");

my $VERSION = "3.0"; 
# change this! KL

print "Dict2Arab: build a new Arabic version of the English-Arabic Technical Dictionary\n";

#get time to be used in version string.
my ($sec,$min,$hour,$mday,$mon,$year,
          $wday,$yday,$isdst) = localtime time;

$mon += 1;
$year += 1900;
$mon  = sprintf("%02d", $mon);
$mday = sprintf("%02d", $mday);
$hour = sprintf("%02d", $hour);
$min  = sprintf("%02d", $min);
$sec  = sprintf("%02d", $sec);

########################################################################
#output the version

print "compute version string ...\n";

open (HANDLE, ">dictvers.tex");
print HANDLE "\\def \\Version{Version: $VERSION.$mday-$mon-$year}\n";
close (HANDLE);

########################################################################
# write tagged data

print "convert input data ...\n";
#<STDIN>;

open (HANDLE, ">arabdata.tex");

#<STDIN>;

#reading the entries
my $line;
while ($line = <RAWTECH>)
{
	my @words = split(/====/, $line);
	my $en = $words[0] ? $words[0] : "-";
	my $ar = $words[1] ? $words[1] : "-";
	
	#encode the arabic tex as arabtex and output it.
	$ar  =~ s/\\n|::|،|\/|\|/, /g;
	chop ($ar);

	print HANDLE "\\Entry{\\En{$en}, \\Ar{$ar}}\n";
}
print HANDLE "\n";
close (HANDLE);
#<STDIN>;

########################################################################
# add keys and headers

print "add sorting keys and header info ...\n";
#<STDIN>;

qx(latex addkeys.tex);

########################################################################
# sort the data

print "sort the Arabic records ...\n";
#<STDIN>;

#open (HANDLE, ">keysort.ctl");
#print HANDLE "arabdata.key arabdict.tex}\n";
#close (HANDLE);

qx(sort arabdata.key -o arabdata.srt);
#qx(latex keysort.tex);

#qx(rm keysort.1);
#qx(rm keysort.2);
#qx(rm keysort.3);
#qx(rm keysort.4);

########################################################################
# compile dictionary

print "build PDF file ... wait ...\n";
#<STDIN>;

qx(pdflatex arabdict.tex);

########################################################################
print "\bDict2Arab done.\n";
exit;
########################################################################






